import {ADD_USER, GET_USERS, PUT_USER, DELETE_USER} from "../Constants/constants";

export const addUser = (data) => ({type: ADD_USER, data});
export const getUsers = (data) => ({type: GET_USERS, data});
export const putUser= (data) => ({type: PUT_USER, data});
export const deleteUser = (data) => ({type: DELETE_USER, data});
import { addUserToBack, getUsersToFront, delUserFromBack, putUserToBack } from "../services/user.service";
import { addUser, getUsers, deleteUser, putUser} from "../actionCreators/action";

export const createUser = (data) => (dispatch) => {addUserToBack(data).then((data) => {dispatch(addUser(data))})};
export const getUsersList = () => (dispatch) => {getUsersToFront().then((data) => {dispatch(getUsers(data))})};
export const delUser = (data) => (dispatch) => {delUserFromBack(data).then((data) => {dispatch(deleteUser(data))})};
export const changeUserName = (data) => (dispatch) => {putUserToBack(data).then((data) => {dispatch(putUser(data))})};

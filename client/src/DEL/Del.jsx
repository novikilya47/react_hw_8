import React, {useState} from 'react';
import {connect} from "react-redux";
import {delUser} from "../thunk/user.thunk"

function GetUser(props) {
    const [email, setEmail] = useState();

    const deleteUser = () => {
        let user = {
            email: email
        };
        props.delUser(user)
    }
        return (
            <div className="Admin">
                <div className = "Admin_block">
                <div>Форма удаления пользователя</div>
                    <div className = "Admin_block_field"><p>Почта пользователя: </p><textarea value={email} onChange={(e) => setEmail(e.target.value)}/></div>
                    <div className="State_buttons">
                            <button onClick={deleteUser}>Удалить пользователя</button>
                    </div>
                    <div>{props.user.count}</div>
                </div>   
            </div>
        )
}

const mapStateToProps = (state) => ({
    user : state.user
});

const mapDispatchToProps = (dispatch) => ({
    delUser: (data) => {dispatch(delUser(data))},
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GetUser);
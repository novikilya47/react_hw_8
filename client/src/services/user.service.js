export async function addUserToBack(data) {
    const resp = await fetch(`http://localhost:3000/user/create`, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })  
    const json = await resp.json();
    return json;
};

export async function getUsersToFront(){
    const resp = await fetch(`http://localhost:3000/user/getAll`, {
        method: 'GET',
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
    const json = await resp.json();
    return json;
};

export async function delUserFromBack(data){
    console.log(data);
    const resp = await fetch(`http://localhost:3000/user/delete`, {
        method: 'DELETE',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
    const json = await resp.json();
    return json;
};

export async function putUserToBack(data) {
    const resp = await fetch(`http://localhost:3000/user/update`, {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
    const json = await resp.json();
    return json;
};
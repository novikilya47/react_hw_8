import React, {useState} from 'react';
import {connect} from "react-redux";
import {getUsersList} from "../thunk/user.thunk";

function GetUser(props) {
    const [showBlock, setShowBlock] = useState(false)

    const userList = () => {
        props.getUsersList();
        setShowBlock(true)
    }
        return (
            <div className="Admin">
                <div className="State_buttons">
                        <button onClick={userList}>Получить список пользователей</button>
                </div>
                {showBlock && props.user.array ? (<div>Список пользователей:</div>) : null}
                {showBlock && props.user.array ? props.user.array.map((array) => (
                    <ul key = {array._id}>
                        <li>
                            Пользователь {array.name};
                            Email: {array.email};
                        </li>
                    </ul>
                )) : null} 
            </div>
        )
}

const mapStateToProps = (state) => ({
    user : state.user
});

const mapDispatchToProps = (dispatch) => ({
    getUsersList: (data) => {dispatch(getUsersList(data))},
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GetUser);
import React, {useState} from 'react';
import {connect} from "react-redux";
import {createUser} from "../thunk/user.thunk";

function SendUser(props) {
    const [name, setName] = useState("dima");
    const [email, setEmail] = useState("dima@maill.ru");

    const sendNewUser = () => {
        let user = {
            name: name,
            email: email
        };

        props.createUser(user)

        setName('');
        setEmail('');
    }

        return (
            <div className = "Admin">
                <div className = "Admin_block">
                    <div>Форма добавления пользователя</div>
                    <div className = "Admin_block_field"><p>Имя пользователя: </p><textarea value={name} onChange={(e) => setName(e.target.value)}/></div>
                    <div className = "Admin_block_field"><p>Почта пользователя: </p><textarea value={email} onChange={(e) => setEmail(e.target.value)}/></div>
                    <div className="State_buttons">
                        <button onClick={sendNewUser}>Отправить</button>
                    </div> 
                    <div>{props.user.message1}</div>                  
                </div>
            </div>
        )
}

const mapStateToProps = (state) => ({
    user : state.user
});

const mapDispatchToProps = (dispatch) => ({
    createUser: (data) => {dispatch(createUser(data))},
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SendUser);

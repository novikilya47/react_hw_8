import {ADD_USER, GET_USERS, DELETE_USER, PUT_USER} from "../Constants/constants";
const defaultState = {}

export default function reducer (state = defaultState, action){
    switch (action.type) {
        case ADD_USER:
            let newState = {
                message1: action.data,
            };
        return newState;
        case GET_USERS:
            let neState = {
                array: action.data.usersFind
            };
        return neState;
        case DELETE_USER:
            let nState = {
                count: action.data,
            };
        return nState;
        case PUT_USER:
            let State = {
                message2: action.data,
            };
        return State;
        default:
            return state;
    }
};
